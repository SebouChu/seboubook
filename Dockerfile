FROM node:9.2.1-alpine

# Install GitBook
RUN npm install -g gitbook-cli
RUN gitbook install
WORKDIR /srv/docs

# Build
COPY . /srv/docs/
RUN gitbook build
EXPOSE 4000
CMD ["gitbook", "serve"]
